#!/usr/bin/env python3
# Copyright (c) 2019 Pietro Albini <pietro@pietroalbini.org>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import subprocess
import sys
import re

SKIP_COMMITS = [
    # Commits added before time tracking was implemented
    "1f82c934934e6c03ddda9c131ff1cd1fe2173e88",
    "cbd8a4d24d021240f1e4169d8b6ed690350c4af8",
    "15e86d5471169209629b7c8ff462d734a71b4c1d",
    "58fa8f0a65b4935046e28fbe6a435dde95c6551f",
    "53ceccf2c4bd025f02e6c051c1fa0e833baf5093",
    "8f26b24828996ae26dfd2018701c98056319a131",
    "6b051fa850b8ef86ad9b29b70765e72d6b89453b",
]
TRACK_EMAILS = [
    "pietro@pietroalbini.org",
    "ndido98@gmail.com",
]
TARGET_HOURS = 80

_minutes_spent_re = re.compile("^Minutes-Spent: ([0-9]+)$")

def err(msg):
    print(msg, file=sys.stderr)
    exit(1)

def get_log():
    gitlog = subprocess.Popen([
        "git", "log", "--format=__COMMIT__ %H %ae%n%B", "--no-merges",
    ], stdout=subprocess.PIPE)
    stdout, _ = gitlog.communicate()
    if gitlog.returncode != 0:
        err("error: fetching the log failed!")
    return stdout.decode("utf-8")

class Commit:
    def __init__(self, hash, author):
        self.hash = hash
        self.author = author
        self.lines = []
        self.minutes_spent = None

    def add_line(self, line):
        matches = _minutes_spent_re.match(line)
        if matches is not None:
            self.minutes_spent = int(matches.group(1))
        self.lines.append(line)

def parse_log(log):
    current = None

    for line in log.split('\n'):
        if not line.strip():
            continue
        if line.startswith("__COMMIT__"):
            if current is not None:
                yield current
            _header, hash, author = line.split(' ', 3)
            current = Commit(hash, author)
        elif current is None:
            err("error: first log line is not a commit header")
        else:
            current.add_line(line)

    if current is not None:
        yield current

def format_minutes(minutes):
    if minutes >= 60:
        return "%s hours and %s minutes" % (int(minutes / 60), minutes % 60)
    else:
        return "%s minutes" % minutes

def track_time():
    time_spent = {}
    malformed_commits = []

    for commit in parse_log(get_log()):
        if commit.hash in SKIP_COMMITS or commit.author not in TRACK_EMAILS:
            pass
        elif commit.minutes_spent is None:
            malformed_commits.append(commit)
        else:
            if commit.author not in time_spent:
                time_spent[commit.author] = 0
            time_spent[commit.author] += commit.minutes_spent

    if malformed_commits:
        print("error: there are commits without a proper Minutes-Spent line:")
        for commit in malformed_commits:
            print("%s: %s" % (commit.hash, commit.lines[0]))
        exit(1)
    else:
        print("time spent by commit author:")
        for author, minutes in time_spent.items():
            print("%s: %s (%s%% of the expected %s hours)" % (author,
                format_minutes(minutes), int(minutes * 100 / (TARGET_HOURS *
                    60)), TARGET_HOURS))

if __name__ == "__main__":
    track_time()
