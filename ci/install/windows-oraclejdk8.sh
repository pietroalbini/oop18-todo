#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

choco install jdk8 --version "8.0.${ORACLEJDK_VERSION}"
export PATH="$PATH:/c/Program Files/Java/jdk1.8.0_${ORACLEJDK_VERSION}/bin"

ci/install/common.sh
