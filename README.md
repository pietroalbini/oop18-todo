# //TODO

//TODO è un videogioco fortemente ispirato a [Human Resource Machine][hrm],
realizzato come progetto per il corso di Programmazione ad Oggetti 2018-2019
(Laurea in Ingegneria e Scienze Informatiche, Università di Bologna).

Il contenuto di questo repository è rilasciato sotto licenza MIT. Vedere il
file `LICENSE` per maggiori dettagli.

[hrm]: https://tomorrowcorporation.com/humanresourcemachine

## Build

Il progetto utilizza gradle per gestire le dipendenze ed effettuare build. Per
eseguire il gioco in locale:

```
$ ./gradlew run
```

Per generare un JAR (che si troverà in `build/libs/todo.jar`):

```
$ ./gradlew jar
```

Per eseguire i test automatizzati:

```
$ ./gradlew test
```
