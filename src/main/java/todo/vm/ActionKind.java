package todo.vm;

public enum ActionKind {
    PICK_INPUT,
    DROP_OUTPUT,
    DROP_MAIN_HAND,
    LOCATE_MEMORY_ADDRESS,
    COPY_FROM,
    COPY_TO,
    ADD,
    SUB,
    INCR,
    DECR,
    NONE;
}
