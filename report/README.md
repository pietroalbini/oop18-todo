# Relazione

Questa directory contiene i sorgenti in LaTeX per la relazione.

Su Ubuntu è necessario installare queste dipendenze per convertire il sorgente
in PDF:

```
$ sudo apt install make inkscape texlive texlive-latex-extra texlive-lang-italian
```

È poi sufficente invocare `make` per trovare il pdf in `build/report.pdf`.
